use syn::{
    Error, ImplItem, ImplItemConst, ImplItemMethod, ImplItemType, TraitItem, TraitItemConst,
    TraitItemMethod, TraitItemType,
};

pub fn impl_block(item: ImplItem) -> Option<Result<TraitItem, Error>> {
    match item {
        ImplItem::Const(item) => Some(convert_const(item)),
        ImplItem::Method(item) => Some(convert_method(item)),
        ImplItem::Type(item) => Some(convert_type(item)),
        ImplItem::Existential(_item) => None,
        ImplItem::Macro(_item) => None,
        ImplItem::Verbatim(_item) => None,
    }
}

fn convert_method(item: ImplItemMethod) -> Result<TraitItem, Error> {
    let ImplItemMethod {
        attrs,
        vis,
        defaultness,
        sig,
        block,
    } = item;
    let default = None;
    let semi_token = Some(Default::default());
    let _ = block;

    check::vis(vis)?;
    check::defaultness(defaultness)?;

    let result = TraitItemMethod {
        attrs,
        sig,
        default,
        semi_token,
    };
    Ok(result.into())
}

fn convert_type(item: ImplItemType) -> Result<TraitItem, Error> {
    let ImplItemType {
        attrs,
        vis,
        defaultness,
        type_token,
        ident,
        generics,
        eq_token,
        ty,
        semi_token,
    } = item;
    let colon_token = None;
    let bounds = Default::default();
    let default = None;
    let _ = eq_token;
    let _ = ty;

    check::vis(vis)?;
    check::defaultness(defaultness)?;

    let result = TraitItemType {
        attrs,
        type_token,
        ident,
        generics,
        colon_token,
        bounds,
        default,
        semi_token,
    };
    Ok(result.into())
}

fn convert_const(item: ImplItemConst) -> Result<TraitItem, Error> {
    let ImplItemConst {
        attrs,
        vis,
        defaultness,
        const_token,
        ident,
        colon_token,
        ty,
        eq_token,
        expr,
        semi_token,
    } = item;
    let default = Some((eq_token, expr));

    check::vis(vis)?;
    check::defaultness(defaultness)?;

    let result = TraitItemConst {
        attrs,
        const_token,
        ident,
        colon_token,
        ty,
        default,
        semi_token,
    };
    Ok(result.into())
}

mod check {
    use crate::error;
    use syn::{spanned::Spanned, Error, Token, Visibility};

    pub fn vis(vis: Visibility) -> Result<(), Error> {
        match vis {
            syn::Visibility::Inherited => Ok(()),
            v => error(v.span(), "No visibility allowed in this context."),
        }
    }

    pub fn defaultness(defaultness: Option<Token![default]>) -> Result<(), Error> {
        match defaultness {
            Some(d) => error(d.span(), "`default` is not allowed in this context."),
            None => Ok(()),
        }
    }
}
