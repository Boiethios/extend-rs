//! This crate provides the `extend` attribute that permits to write easier extensions
//! to foreign types.
//!
//! Give the name of the extension trait as the attribute parameter (with an optional visibility
//! modifier), and the trait will be automagically generated.
//!
//! # Example with foreign type
//!
//! ```
//! use extend::*;
//!
//! #[extend(StringJoiner)]
//! impl<'a> Vec<&'a str> {
//!     fn join_with_comma(&self) -> String {
//!         let mut it = self.iter();
//!         let first = it.next().map(|&s| s.to_owned()).unwrap_or_default();
//!         it.fold(first, |whole, s| whole + ", " + s)
//!     }
//! }
//!
//! assert_eq!(
//!     vec!["hello", "world", "!"].join_with_comma(),
//!     "hello, world, !"
//! );
//! ```
//!
//! # Example with generic type
//!
//! ```
//! use extend::*;
//!
//! #[extend(IteratorUnique)]
//! impl<T> T
//! where
//!     T: IntoIterator + Sized,
//! {
//!     type Item = T::Item;
//!
//!     fn unique(self) -> Option<Self::Item> {
//!         let mut it = self.into_iter();
//!         let next = it.next();
//!         match it.next() {
//!             Some(_) => None,
//!             None => next,
//!         }
//!     }
//! }
//!
//! assert_eq!(vec![1].unique(), Some(1));
//! assert_eq!(vec![1, 2].unique(), None);
//! ```
//!
//! # Example with visibility modifier
//!
//! ```
//! use extend::*;
//!
//! #[extend(pub OptionExt)]
//! impl Option<bool> {
//!     fn is_true(self) -> bool {
//!         self.unwrap_or(false)
//!     }
//! }
//!
//! assert_eq!(Some(true).is_true(), true);
//! assert_eq!(Some(false).is_true(), false);
//! assert_eq!(Option::None::<bool>.is_true(), false);
//! ```

extern crate proc_macro;

mod transform;

use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, parse_quote, spanned::Spanned, Error, Ident, ItemImpl, Visibility};

/// Debugs if the "debug" feature is activated.
macro_rules! debug {
    ($e:expr) => {{
        #[cfg(feature = "debug")]
        {
            println!(
                "\x1B[32m====================== {} ======================",
                stringify!($e)
            );
            println!("{}", $e);
            println!("\x1B[32m======================================");
        }
    }};
}

/// See the documentation of the crate.
#[proc_macro_attribute]
pub fn extend(args: TokenStream, input: TokenStream) -> TokenStream {
    debug!(input);

    let output = do_the_job(args, parse_macro_input!(input as ItemImpl))
        .unwrap_or_else(|e| e.to_compile_error().into());

    debug!(output);
    output
}

fn do_the_job(args: TokenStream, mut input: ItemImpl) -> Result<TokenStream, Error> {
    if let Some(t) = &input.trait_ {
        return error(
            t.1.span(),
            "Write your implementation as if it is inherent.",
        );
    }
    let (vis, ident) = trait_info(args.into())?;
    let declarations_ = input
        .items
        .iter()
        .cloned()
        .filter_map(transform::impl_block);
    let mut declarations = Vec::new();

    for declaration in declarations_ {
        declarations.push(declaration?);
    }
    input.trait_ = Some((None, parse_quote!(#ident), Default::default()));

    let output = quote! {
        #vis trait #ident {
            #( #declarations )*
        }

        #input
    };

    Ok(output.into())
}

/// Recover the trait information from the arguments.
fn trait_info(stream: pm::TokenStream) -> Result<(Visibility, Ident), Error> {
    fn parse_vis(ident: pm::Ident) -> Result<Visibility, Error> {
        let it = std::iter::once(pm::TokenTree::from(ident));
        let stream = pm::TokenStream::from_iter(it).into();

        syn::parse(stream)
    }

    use std::iter::FromIterator;

    let span = stream.span();
    let mut it = stream.into_iter();

    match it.next() {
        Some(pm::TokenTree::Ident(ident)) => {
            if let Ok(vis) = parse_vis(ident.clone()) {
                match it.next() {
                    Some(pm::TokenTree::Ident(ident)) => Ok((vis, ident)),
                    opt => error(
                        opt.map(|t| t.span()).unwrap_or_else(|| vis.span()),
                        "Expected identifier after the visibility modifier.",
                    ),
                }
            } else {
                match it.next() {
                    None => Ok((Visibility::Inherited, ident)),
                    Some(t) => error(t.span(), "Unexpected token."),
                }
            }
        }
        opt => error(
            opt.map(|t| t.span()).unwrap_or(span),
            "Expected: visibility modifier or identifier.",
        ),
    }
}

fn error<T>(span: pm::Span, message: &str) -> Result<T, Error> {
    Err(Error::new(span, message))
}
